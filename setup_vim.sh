#!/bin/bash

SCRIPT_HOME=$(pwd)
# Install fzf
echo -e "Installing fzf (needed for fzf plugin)\n"
if [ ! -d ~/.fzf ]; then
	git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
else 
	cd ~/.fzf && git pull
fi

~/.fzf/install \
	--key-bindings \
	--completion \
 	--no-fish \
	--update-rc

# Copying .vimrc file
if [ -f ~/.vimrc ]; then
	BACKUP_FILEPATH=~/.vimrc.OLD
	echo '~/.vimrc already exists'
	echo -e "Creating a backup at: ${BACKUP_FILEPATH}\n"
	cp ~/.vimrc $BACKUP_FILEPATH
fi

cp $SCRIPT_HOME/config/vimrc ~/.vimrc

# Install Vundle and dependencies
echo -e "Installing Vundle and plugins\n"
if [ ! -d ~/.vim/bundle/Vundle.vim ]; then
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
else
	cd ~/.vim/bundle/Vundle.vim && git pull
fi
vim +PluginInstall +qall

echo -e "Removing unused plugins"
vim +PluginClean! +qall

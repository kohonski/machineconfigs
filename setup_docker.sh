set -e

apt-get update

# Install packages to enable apt to use a repository over HTTPS
sudo apt-get install -y \
	  apt-transport-https \
		ca-certificates \
		curl \
		gnupg-agent \
		software-properties-common

# Add Docker's offical GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

# Verify key is added
apt-key fingerprint 0EBFCD88

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update

sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# Verify install worked
command -v docker >/dev/null 2>&1

# Instal docker-compose
if [ ! -f /usr/local/bin/docker-compose ]; then
	curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

	chmod +x /usr/local/bin/docker-compose
fi

command -v docker-compose >/dev/null 2>&1
